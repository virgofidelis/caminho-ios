//
//  TextoHeaderTableViewCell.swift
//  Caminho
//
//  Created by Jonathan Loureiro on 23/07/2018.
//  Copyright © 2018 escrivaworks. All rights reserved.
//

import UIKit

class TextoHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak private var label: UILabel!

    var header: String! {
        didSet {
            self.label.text = self.header.uppercased()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
