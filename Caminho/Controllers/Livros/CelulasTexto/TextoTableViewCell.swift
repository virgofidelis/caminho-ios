//
//  TextoTableViewCell.swift
//  Caminho
//
//  Created by Jonathan Loureiro on 23/07/2018.
//  Copyright © 2018 escrivaworks. All rights reserved.
//

import UIKit

class TextoTableViewCell: UITableViewCell {

    @IBOutlet weak private var label: UILabel!
    
    var textCell: String! {
        didSet {
            self.label.text = self.textCell
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
