//
//  TextoViewController.swift
//  Caminho
//
//  Created by Jonathan Loureiro on 19/07/2018.
//  Copyright © 2018 escrivaworks. All rights reserved.
//

import UIKit

class TextoViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var numberOfSections: Int = 0
    var bookTitle: String!
    
    struct id {
        static let cell = "TextoTableViewCell"
        static let header = "TextoHeaderTableViewCell"
    }
    
    var chapterPoints: [Database.ChapterPoints]! {
        didSet {
            self.numberOfSections = chapterPoints.count
        }
    }
    
    var chapterTitle: String! {
        didSet {
            self.setNavigationTitle(title: self.chapterTitle)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

}


extension TextoViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: id.cell, for: indexPath) as! TextoTableViewCell
        cell.textCell = "\(self.chapterPoints[indexPath.section].text)"
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.numberOfSections
    }
}

extension TextoViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: id.header) as! TextoHeaderTableViewCell
        cell.header = "\(self.bookTitle!) \(self.chapterPoints[section].point)"
        return cell.contentView
    }
    
}
