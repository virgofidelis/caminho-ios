//
//  CapitulosTableViewCell.swift
//  Caminho
//
//  Created by Jonathan Loureiro on 23/07/2018.
//  Copyright © 2018 escrivaworks. All rights reserved.
//

import UIKit

class CapitulosTableViewCell: UITableViewCell {

    @IBOutlet weak private var numLabel: UILabel!
    @IBOutlet weak private var titleLabel: UILabel!
    
    var numberChapter: String! {
        didSet {
            self.numLabel.text = numberChapter
        }
    }
    
    var titleChapter: String! {
        didSet {
            self.titleLabel.text = titleChapter
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
