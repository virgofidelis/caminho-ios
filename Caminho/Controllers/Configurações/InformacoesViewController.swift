//
//  ConfiguracaoViewController.swift
//  Caminho
//
//  Created by Jonathan Loureiro on 18/07/2018.
//  Copyright © 2018 escrivaworks. All rights reserved.
//

import UIKit

class InformacoesViewController: UIViewController {
    
    struct id {
        static let cell: String = "InformacoesTableViewCell"
        static let header: String = "InformacoesHeaderTableViewCell"
    }
    
    struct About {
        let header: String
        let rows: [Row]
    }
    
    struct Row {
        let text: String
        let url: URL?
    }
    
    fileprivate let about: [About] = [
        About(header: "Aplicativo totalmente offline", rows:
            [
                Row(
                    text: "Texto alertando que se os favoritos estão salvos apenas no aplicativo, eles sumirão caso apague o aplicativo ou perca o smartphone.",
                    url: nil
                )
            ]),
        About(header: "Observação", rows:
            [
                Row(
                    text: "O conteúdo apresentado neste aplicativo tem todos os direitos de propriedade intelectual legalmente reservados a Studium Foundation.",
                    url: URL(string: "http://www.escrivaworks.org/doc/studium")
                )
            ]),
        About(header: "Licenças", rows:
            [
                Row(
                    text: "NPOSL 3.0",
                    url: URL(string: "https://opensource.org/licenses/NPOSL-3.0")
                ),
                Row(
                    text: "CC BY-NC-SA 4.0",
                    url: URL(string: "https://creativecommons.org/licenses/by-nc-sa/4.0")
                )
            ])
        ]
    
    @IBOutlet weak private var versionLabel: UILabel!
    
    private var version: String! {
        didSet {
            self.versionLabel.text = self.version
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupVersion()
        self.setNavigationTitle(title: "Sobre este app")
    }
    
    private func setupVersion() {
        if let version = Bundle.main.infoDictionary!["CFBundleShortVersionString"] {
            self.version = "Version \(version)"
        } else {
            self.version = ""
        }
    }
    
}

extension InformacoesViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.about[section].rows.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: id.cell, for: indexPath) as! InformacoesTableViewCell
        cell.textCell = self.about[indexPath.section].rows[indexPath.row].text
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.about.count
    }
    
}

extension InformacoesViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if let url = self.about[indexPath.section].rows[indexPath.row].url {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: id.header) as! InformacoesHeaderTableViewCell
        cell.header = self.about[section].header
        return cell.contentView
    }
    
}
