//
//  File.swift
//  Caminho
//
//  Created by Jonathan Loureiro on 23/07/2018.
//  Copyright © 2018 escrivaworks. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func setNavigationTitle(title: String, backTitle: String) {
        self.setNavigationTitle(title: title)
        self.setNavigationBackTitle(backTitle: backTitle)
    }
    
    func setNavigationTitle(title: String) {
        self.navigationItem.title = title
    }
    
    func setNavigationBackTitle(backTitle: String) {
        if let backBarButtonItem = self.navigationItem.backBarButtonItem {
            backBarButtonItem.title = backTitle
        }
    }
    
}
